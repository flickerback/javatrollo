package sample;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class TaskDAO {
    private SimpleObjectProperty<Task> currentTask=new SimpleObjectProperty<>();
    private SimpleStringProperty name=new SimpleStringProperty();
    private SimpleStringProperty description=new SimpleStringProperty();
    private SimpleStringProperty duedate=new SimpleStringProperty();

    private static TaskDAO dao=null;
    public static synchronized TaskDAO getInstance(){
        if (dao==null) dao=new TaskDAO();
        return dao;
    }

    private TaskDAO(){

        currentTask.addListener(
                new ChangeListener<Task>() {
                    @Override
                    public void changed(ObservableValue<? extends Task> observable, Task oldValue, Task newValue) {
                        setCurrentTask(newValue);
                    }
                }
        );
    }

    public void setTask(Task currentTask) {
        this.currentTask.set(currentTask);
    }

    private void setCurrentTask(Task t){
        name.set(t.getName());
        description.set(t.getDescription());
        duedate.set(t.getDuedate());
    }

    public SimpleObjectProperty<Task> getCurrentTask(){return currentTask;}

    public SimpleStringProperty getName() {
           return name;
    }

    public SimpleStringProperty getDescription() {
        return description;
    }

    public SimpleStringProperty getDuedate() {
        return duedate;
    }

}


