package sample;
import java.time.LocalDate;

public class Task {
    private String name;
    private String description="";
    private int finished;
    static final int displaylength=30;
    private String duedate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFinished() {
        return finished;
    }

    public void setFinished(Integer finished) {
        this.finished = finished;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    @Override
    public String toString(){
        String namedescription=name+" - "+description;

        if (namedescription.length()<displaylength){
            return namedescription;
        }
        else{
            return namedescription.substring(0,displaylength)+"...";
        }
    }

    public String taskToString(){
        String result=new String();
        result=this.name+";"+this.description+";"+this.finished+";"+this.duedate;
        return result;
    }
}
