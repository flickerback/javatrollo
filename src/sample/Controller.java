package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.function.Function;

public class Controller implements Initializable {

    TaskDAO dao=TaskDAO.getInstance();

    @FXML
    ListView<Task> list_todo;
    @FXML
    ListView<Task> list_done;
    @FXML
    Button btn_add;
    @FXML
    Button btn_remove;
    @FXML
    Button btn_resolve;
    @FXML
    Button btn_reopen;
    @FXML
    TextField txt_addtask;
    @FXML
    TextArea txt_description;
    @FXML
    MenuItem menu_savetofile;
    @FXML
    MenuItem menu_loadfromfile;
    @FXML
    AnchorPane pane;
    @FXML
    MenuItem menu_sort_ab_asc;
    @FXML
    MenuItem menu_sort_ab_desc;
    @FXML
    Label lbl_duedate;
    @FXML
    DatePicker dpicker_duedate;
    @FXML
    Button btn_clearDate;
    @FXML
    MenuItem menu_sortbydate_asc;
    @FXML
    MenuItem menu_sortbydate_desc;

    @Override

    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/sample/tasks.txt");
        readFromFile(file);

        btn_resolve.setOnMouseClicked(this::onResolveClicked);
        btn_reopen.setOnMouseClicked(this::onReopenClicked);
        btn_add.setOnMouseClicked(this::onAddClicked);
        btn_remove.setOnMouseClicked(this::onRemoveClicked);
        btn_clearDate.setOnMouseClicked(this::onClearDateClicked);

        list_todo.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        list_done.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        list_todo.setOnMouseClicked(this::onTodoClicked);
        list_done.setOnMouseClicked(this::onDoneClicked);

        menu_savetofile.setOnAction(this::onSaveToFile);
        menu_loadfromfile.setOnAction(this::onLoadFromFile);
        menu_sort_ab_asc.setOnAction(this::onSortAbAsc);
        menu_sort_ab_desc.setOnAction(this::onSortAbDesc);
        menu_sortbydate_asc.setOnAction(this::onSortDateAsc);
        menu_sortbydate_desc.setOnAction(this::onSortDateDesc);

        ChangeListener<Task> taskListener = new ChangeListener<Task>() {
            @Override
            public void changed(ObservableValue<? extends Task> observable, Task oldValue, Task newValue) {
                if(newValue!=null){
                    txt_description.setDisable(false);
                    txt_description.setText(newValue.getDescription());
                    dpicker_duedate.setOnAction(null);
                    if (newValue.getDuedate().equals("0")){
                        dpicker_duedate.getEditor().clear();
                        lbl_duedate.setText(" ");
                        newValue.setDuedate("0");
                    }
                    else{
                        dpicker_duedate.setValue(fromString(newValue.getDuedate()));
                        lbl_duedate.setText(newValue.getDuedate());
                    }
                    dao.setTask(newValue);
                    dpicker_duedate.setOnAction(Controller.this::onDateChanged);
                }
            }
        };

        list_todo.getSelectionModel().selectedItemProperty().addListener(taskListener);
        list_done.getSelectionModel().selectedItemProperty().addListener(taskListener);

        txt_description.setOnKeyReleased(this::onDescriptionChanged);

    }
    void onResolveClicked(MouseEvent mouseEvent){
        ObservableList<Task> selectedItems=list_todo.getSelectionModel().getSelectedItems();
        for (Task task: selectedItems){
            task.setFinished(1);
        }
        list_done.getItems().addAll(selectedItems);
        list_todo.getItems().removeAll(selectedItems);
    }
    void onReopenClicked(MouseEvent mouseEvent){
        ObservableList<Task> selectedItems=list_done.getSelectionModel().getSelectedItems();
        for (Task task: selectedItems){
            task.setFinished(0);
        }
        list_todo.getItems().addAll(selectedItems);
        list_done.getItems().removeAll(selectedItems);
    }

    void onAddClicked(MouseEvent mouseEvent){
        String title=txt_addtask.getText();
        if (title!=null){
            title=title.trim();
            if(title.length()!=0){
                Task new_task=new Task();
                new_task.setName(txt_addtask.getText());
                new_task.setFinished(0);
                list_todo.getItems().add(new_task);
                new_task.setDuedate("0");
            }
        }
        txt_addtask.clear();
    }

    void onRemoveClicked(MouseEvent mouseEvent){

        ObservableList<Task> selectedItems_todo=list_todo.getSelectionModel().getSelectedItems();
            list_todo.getItems().removeAll(selectedItems_todo);

        ObservableList<Task> selectedItems_done=list_done.getSelectionModel().getSelectedItems();
            list_done.getItems().removeAll(selectedItems_done);

            list_todo.getSelectionModel().clearSelection();
            list_done.getSelectionModel().clearSelection();
            txt_description.setText("");
            lbl_duedate.setText("");
            dpicker_duedate.getEditor().clear();
        }

    void onClearDateClicked(MouseEvent mouseEvent){
        dpicker_duedate.getEditor().clear();
        if(dao.getCurrentTask().get()!=null){
            Task t=dao.getCurrentTask().get();
            t.setDuedate("0");
            lbl_duedate.setText(" ");
            dao.setTask(t);
        }
    }

    void onTodoClicked(MouseEvent mouseEvent){
        list_done.getSelectionModel().clearSelection();
    }
    void onDoneClicked(MouseEvent mouseEvent){
        list_todo.getSelectionModel().clearSelection();
    }

    void onDescriptionChanged(KeyEvent keyEvent){
        String description=txt_description.getText();
        if(dao.getCurrentTask().get()!=null){
            Task t=dao.getCurrentTask().get();
            t.setDescription(description);
            dao.setTask(t);
            list_todo.refresh();
            list_done.refresh();
        }
    }

    void onDateChanged(ActionEvent actionEvent) {
        String date=dpicker_duedate.getValue().toString();
        if(dao.getCurrentTask().get()!=null){
                Task t=dao.getCurrentTask().get();
                t.setDuedate(date);
                lbl_duedate.setText(t.getDuedate());
                dao.setTask(t);
        }
    }

    public void readFromFile(File file){
        try {
            Scanner sc=new Scanner(file);
            String line;
            ObservableList<Task> tasklist_todo= FXCollections.observableArrayList();
            ObservableList<Task> tasklist_done= FXCollections.observableArrayList();

            while(sc.hasNext()){
                line=sc.nextLine();
                String task[]=line.split(";");
                Task newtask=new Task();
                newtask.setName(task[0]);
                newtask.setDescription(task[1]);
                newtask.setFinished(Integer.parseInt(task[2]));
                newtask.setDuedate(task[3]);
                if(Integer.parseInt(task[2])==0){
                    tasklist_todo.add(newtask);
                }
                else if(Integer.parseInt(task[2])==1){
                    tasklist_done.add(newtask);
                }
            }
            list_todo.setItems(tasklist_todo);
            list_done.setItems(tasklist_done);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    void onSaveToFile(ActionEvent actionEvent){
    try {
        BufferedWriter writer = new BufferedWriter(new FileWriter("SavedTasks.txt"));
        ObservableList<Task> tasklist= FXCollections.observableArrayList();
        tasklist.addAll(list_todo.getItems());
        tasklist.addAll(list_done.getItems());

      for(Task task : tasklist){
          writer.write(task.taskToString());
          writer.newLine();
      }
        writer.close();

        File file = new File("SavedTasks.txt");
        String path = file.getAbsolutePath();
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setHeaderText("Successfully saved to file!");
        a.setTitle("File saved");
        a.setContentText(path);
        a.show();
    }
    catch(Exception ex){

        }
    }

     void onLoadFromFile(ActionEvent actionEvent) {
        Window window = pane.getScene().getWindow();
        FileChooser fc=new FileChooser();
        fc.setTitle("Choose the file: ");
        FileChooser.ExtensionFilter filter=new FileChooser.ExtensionFilter("TXT files","*.txt");
        fc.getExtensionFilters().add(filter);
        File odabranaDatoteka=fc.showOpenDialog(window);
        if(odabranaDatoteka!=null){
            readFromFile(odabranaDatoteka);
        }
    }

    void onSortAbAsc(ActionEvent actionEvent) {
        ObservableList<Task> items_todo=list_todo.getItems();
        ObservableList<Task> items_done=list_done.getItems();
        FXCollections.sort(items_todo,comparatorTask_byName);
        FXCollections.sort(items_done,comparatorTask_byName);
    }

    void onSortAbDesc(ActionEvent actionEvent) {
        ObservableList<Task> items_todo=list_todo.getItems();
        ObservableList<Task> items_done=list_done.getItems();
        FXCollections.sort(items_todo,comparatorTask_byName.reversed());
        FXCollections.sort(items_done,comparatorTask_byName.reversed());
    }

    void onSortDateAsc(ActionEvent actionEvent) {
        ObservableList<Task> items_todo=list_todo.getItems();
        ObservableList<Task> items_done=list_done.getItems();
        FXCollections.sort(items_todo,comparatorTask_byDate);
        FXCollections.sort(items_done,comparatorTask_byDate);
    }

    void onSortDateDesc(ActionEvent actionEvent) {
        ObservableList<Task> items_todo=list_todo.getItems();
        ObservableList<Task> items_done=list_done.getItems();
        FXCollections.sort(items_todo,comparatorTask_byDate.reversed());
        FXCollections.sort(items_done,comparatorTask_byDate.reversed());
    }

    Comparator<? super Task> comparatorTask_byName = new Comparator<Task>() {
        @Override
        public int compare(Task o1, Task o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    };

    Comparator<? super Task> comparatorTask_byDate = new Comparator<Task>() {
        @Override
        public int compare(Task o1, Task o2) {
            return o1.getDuedate().compareToIgnoreCase(o2.getDuedate());
        }
    };

    public LocalDate fromString(String string) {
        try {
            return LocalDate.parse(string);
        } catch (DateTimeParseException exc) {
            return null ;
        }
    }
}


